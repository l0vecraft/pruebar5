part of pruebar5.core;

final _fbInstance = FirebaseFirestore.instance;
final _fireBaseServices = FirebaseServiceImpl(instance: _fbInstance);

final todoProvider = StateNotifierProvider<TodoController, TodoState>((ref) {
  return TodoController(api: _fireBaseServices);
});
