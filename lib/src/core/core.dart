library pruebar5.core;

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pruebar5/src/core/model/models.dart';
import 'package:pruebar5/src/core/repository/repository.dart';
import 'package:pruebar5/src/core/service/services.dart';

part 'controller/todo_controller.dart';
part 'controller/todo_state.dart';

part 'providers.dart';
