part of pruebar5.core.service;

class FirebaseServiceImpl implements FirebaseRepository {
  final FirebaseFirestore instance; //debe de mandarse el nombre de la db
  const FirebaseServiceImpl({required this.instance});

  @override
  Future<bool> createTodo(TodoModel todo) async {
    try {
      await instance.collection('todo').add(todo.toJson());
      return true;
    } catch (e) {
      throw Failure(title: 'Error al crear', message: e.toString());
    }
  }

  @override
  Future<bool> delteTodo(String id) async {
    try {
      await instance.collection('todo').doc(id).delete();
      return true;
    } catch (e) {
      throw Failure(title: 'Error al borrar', message: e.toString());
    }
  }

  @override
  Future<bool> updateTodo(String id, TodoModel newTodo) async {
    try {
      var todoToUpdate = instance.collection('todo').doc(id);
      await todoToUpdate.update(newTodo.toJson());
      return true;
    } catch (e) {
      throw Failure(title: 'Error al actualizar', message: e.toString());
    }
  }

  @override
  Future<List<TodoModel>> getTodos() async {
    var listOfTodos = <TodoModel>[];
    try {
      var result = await instance.collection('todo').get();
      listOfTodos = result.docs
          .map((todo) => TodoModel.fromMap(todo.data(), todo.id))
          .toList();
      return listOfTodos;
    } catch (e) {
      throw Failure(title: 'Error al leer', message: e.toString());
    }
  }
}
