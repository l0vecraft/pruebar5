part of pruebar5.core;

class TodoState {
  TodoState({
    this.listOfTodos,
    this.filterTodos,
  });

  final List<TodoModel>? listOfTodos;
  final List<TodoModel>? filterTodos;

  int get todosDone => listOfTodos?.where((todo) => todo.hasDone).length ?? 0;

  TodoState copyWith({
    List<TodoModel>? listOfTodos,
    List<TodoModel>? filterTodos,
  }) =>
      TodoState(
        listOfTodos: listOfTodos ?? this.listOfTodos,
        filterTodos: filterTodos ?? [],
      );
}
