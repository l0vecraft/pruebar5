part of pruebar5.core;

class TodoController extends StateNotifier<TodoState> {
  final FirebaseRepository api;

  TodoController({required this.api}) : super(TodoState(listOfTodos: []));

  void addTodo(String todo) async {
    try {
      var result = state.listOfTodos ?? [];
      var newTodo =
          TodoModel(title: todo, date: DateTime.now().millisecondsSinceEpoch);
      var res = await api.createTodo(newTodo);
      if (res) {
        result.add(newTodo);
        state = state.copyWith(listOfTodos: result);
      }
    } on Failure catch (error) {
      log("${error.title}=> ${error.message}");
    } on Exception catch (_) {
      rethrow;
    }
  }

  void getAllTodos() async {
    try {
      var res = await api.getTodos();
      state = state.copyWith(listOfTodos: res);
    } on Failure catch (error) {
      log("${error.title}=> ${error.message}");
    } on Exception catch (_) {
      rethrow;
    }
  }

  void doneTask(int index) async {
    try {
      state.listOfTodos?[index].hasDone = !state.listOfTodos![index].hasDone;
      var res = await api.updateTodo(
          state.listOfTodos?[index].id ?? '', state.listOfTodos![index]);
      if (res) {
        state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
      }
    } on Failure catch (error) {
      log("${error.title}=> ${error.message}");
    } on Exception catch (_) {
      rethrow;
    }
  }

  void editTask(int index, String title, String? description) async {
    try {
      state.listOfTodos?[index].title = title;
      state.listOfTodos?[index].description = description;
      var res = await api.updateTodo(
          state.listOfTodos?[index].id ?? '', state.listOfTodos![index]);
      if (res) {
        state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
      }
    } on Failure catch (error) {
      log("${error.title}=> ${error.message}");
    } on Exception catch (_) {
      rethrow;
    }
  }

  void removeTask(int index) async {
    try {
      var deletedTodo = state.listOfTodos?.removeAt(index);
      var res = await api.delteTodo(deletedTodo?.id ?? '');
      if (res) {
        state = state.copyWith(listOfTodos: state.listOfTodos ?? []);
      }
    } on Failure catch (error) {
      log("${error.title}=> ${error.message}");
    } on Exception catch (_) {
      rethrow;
    }
  }
}
