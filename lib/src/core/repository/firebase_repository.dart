part of pruebar5.core.repository;

abstract class FirebaseRepository {
  Future<bool> createTodo(TodoModel todo);
  Future<bool> delteTodo(String id);
  Future<bool> updateTodo(String id, TodoModel newTodo);
  Future<List<TodoModel>> getTodos();
}
