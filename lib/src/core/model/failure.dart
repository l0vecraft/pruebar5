part of pruebar5.core.models;

class Failure implements Exception {
  final String title;
  final String? message;

  const Failure({required this.title, this.message});
}
