part of pruebar5.core.models;

class TodoModel {
  TodoModel({
    this.id,
    required this.title,
    this.description,
    required this.date,
    this.hasDone = false,
  });
  String? id;
  String title;
  String? description;
  bool hasDone;
  int date;

  factory TodoModel.fromJson(String todo, String id) =>
      TodoModel.fromMap(json.decode(todo), id);
  factory TodoModel.fromMap(Map<String, dynamic> todo, String id) => TodoModel(
      id: id,
      title: todo['title'],
      date: todo['date'],
      description: todo['description'],
      hasDone: todo['hasDone']);

  Map<String, dynamic> toJson() => {
        "title": title,
        "date": date,
        "description": description,
        "hasDone": hasDone
      };
}
