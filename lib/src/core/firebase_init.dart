import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class FireBaseInit {
  static final FireBaseInit _instance = FireBaseInit._();

  factory FireBaseInit() => _instance;

  FireBaseInit._();

  DatabaseReference get dbReference => FirebaseDatabase.instance.ref();

  Future<void> initialize() async {
    await Firebase.initializeApp();
  }
}
