library pruebar5.ui.screen;

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pruebar5/src/core/core.dart';
import 'package:pruebar5/src/ui/styles/styles.dart';
import 'package:pruebar5/src/ui/widget/widgets.dart';

part 'home_screen.dart';
