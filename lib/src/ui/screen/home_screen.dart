part of pruebar5.ui.screen;

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  final _focusTextInput = FocusNode();
  @override
  Widget build(BuildContext context) {
    final state = ref.watch(todoProvider);
    final viewState = ref.read(todoProvider.notifier);
    viewState.getAllTodos();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo app'),
        centerTitle: true,
        backgroundColor: AppStyle.primaryColor,
      ),
      backgroundColor: AppStyle.backgroundColor,
      body: GestureDetector(
        onTap: () {
          if (_focusTextInput.hasFocus) {
            _focusTextInput.unfocus();
          }
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
          child: CustomScrollView(slivers: [
            SliverList(
              delegate: SliverChildListDelegate([
                const TodoMainCard(),
                SizedBox(height: 15.h),
                TodoFormField(focus: _focusTextInput),
                SizedBox(height: 20.h),
                if (state.listOfTodos == null ||
                    (state.listOfTodos?.isEmpty ?? true)) ...<Widget>[
                  const EmptyTodos()
                ] else ...[
                  TodoContent(listOfTodos: state.listOfTodos)
                ]
              ]),
            ),
          ]),
        ),
      ),
    );
  }
}
