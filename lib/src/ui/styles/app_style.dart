part of pruebar5.styles;

class AppStyle {
  /* COLORS */
  static const secondaryColor = Color(0xff1e1e1e);
  static const primaryColor = Color.fromRGBO(239, 108, 0, 1);
  static const backgroundColor = Color(0xFF0d0d0d);
  static const ligthColor = Color(0xffcebea4);
  static const challengePrimaryColor = Color(0xff8E70FE);
  static const challengeDisableColor = Color(0xffDDD4FF);

  /* STYLES */
  static final counterStyle =
      TextStyle(fontSize: 35.sp, fontWeight: FontWeight.bold);
  static final title1 = TextStyle(
      fontWeight: FontWeight.bold, fontSize: 25.sp, color: AppStyle.ligthColor);
  static const subTitle1 =
      TextStyle(fontWeight: FontWeight.w300, color: AppStyle.ligthColor);
  static const textFieldStyle = TextStyle(color: AppStyle.ligthColor);
  static TextStyle todoItemTitleStyle(bool hasDone) => TextStyle(
      fontSize: 18.sp,
      fontWeight: FontWeight.bold,
      color: AppStyle.ligthColor,
      decoration: hasDone ? TextDecoration.lineThrough : TextDecoration.none);
  static TextStyle todoItemDescriptionStyle(bool hasDone) => TextStyle(
      fontSize: 14.sp,
      color: AppStyle.ligthColor,
      decoration: hasDone ? TextDecoration.lineThrough : TextDecoration.none,
      overflow: TextOverflow.ellipsis);

  static final challengeStepIndicatorStyle =
      GoogleFonts.getFont('Red Hat Display', color: AppStyle.backgroundColor);
  static final challengeTitle1 = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 20.sp,
    fontWeight: FontWeight.bold,
  );
  static final challengeTitleAppBar = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 20.sp,
  );
  static final challengeText1 = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 16.sp,
  );
  static final challengeSelectedCardText = GoogleFonts.getFont(
    'Red Hat Display',
    color: Colors.blue,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
  );
  static final challengeCardTitleText = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
  );
  static final challengeCardSubtitleText = GoogleFonts.getFont(
    'Red Hat Display',
    color: Colors.grey[800],
    fontSize: 12.sp,
  );
  static final challengeQuestionElementsText = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 12.sp,
  );
  static final challengeTabSelectedText = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.challengePrimaryColor,
    fontSize: 14.sp,
  );
  static final challengeTabText = GoogleFonts.getFont(
    'Red Hat Display',
    color: AppStyle.backgroundColor,
    fontSize: 14.sp,
  );
  static final challengeBigNumber = GoogleFonts.getFont(
    'Red Hat Display',
    color: Colors.blue,
    fontSize: 40.sp,
  );
}
