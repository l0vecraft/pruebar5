part of pruebar5.ui.widget;

class CustomButton extends StatelessWidget {
  const CustomButton({
    required this.onPress,
    required this.text,
    this.color = AppStyle.primaryColor,
    this.borderColor,
    this.disabledColor,
    super.key,
  });
  final String text;
  final Color color;
  final Color? borderColor;
  final Color? disabledColor;
  final VoidCallback? onPress;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPress,
      style: ButtonStyle(
        shape: MaterialStateProperty.resolveWith((states) =>
            RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(color: borderColor ?? Colors.transparent))),
        backgroundColor: MaterialStateColor.resolveWith((states) {
          if (states.contains(MaterialState.disabled)) {
            return disabledColor ?? Colors.grey;
          } else {
            return borderColor != null ? AppStyle.backgroundColor : color;
          }
        }),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 12.h),
        child: Text(
          text,
          style: TextStyle(fontSize: 20.sp, color: borderColor ?? Colors.white),
        ),
      ),
    );
  }
}
