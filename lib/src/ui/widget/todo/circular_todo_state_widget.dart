part of pruebar5.ui.widget;

class CircularTodoStateWidget extends StatelessWidget {
  const CircularTodoStateWidget({
    super.key,
    required this.onDone,
    required this.hasDone,
  });

  final VoidCallback onDone;
  final bool hasDone;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: InkWell(
        onTap: onDone,
        borderRadius: BorderRadius.circular(100),
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          alignment: Alignment.center,
          height: 30.h,
          width: 30.w,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              border: Border.all(
                  color: hasDone ? Colors.transparent : AppStyle.primaryColor),
              color: hasDone ? Colors.green : Colors.transparent),
          child: hasDone ? const Icon(Icons.done, color: Colors.white) : null,
        ),
      ),
    );
  }
}
