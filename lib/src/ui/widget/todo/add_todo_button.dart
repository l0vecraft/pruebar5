part of pruebar5.ui.widget;

class AddTodoButton extends StatelessWidget {
  const AddTodoButton({
    required this.onPressed,
    super.key,
  });

  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15.w),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
            minimumSize:
                MaterialStateProperty.resolveWith((states) => Size(0.w, 60.h)),
            shape: MaterialStateProperty.resolveWith(
              (states) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100),
              ),
            ),
            backgroundColor: MaterialStateColor.resolveWith((states) =>
                states.contains(MaterialState.pressed)
                    ? Colors.orange
                    : AppStyle.primaryColor)),
        child: Icon(
          Icons.add,
          color: Colors.black,
          size: 20.sp,
          weight: 220,
        ),
      ),
    );
  }
}
