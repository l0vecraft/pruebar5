part of pruebar5.ui.widget;

class TodoEditDialog extends ConsumerStatefulWidget {
  const TodoEditDialog(
      {required this.titleValue,
      this.descriptionValue,
      required this.index,
      Key? key})
      : super(key: key);

  final String titleValue;
  final String? descriptionValue;
  final int index;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TodoEditDialogState();
}

class _TodoEditDialogState extends ConsumerState<TodoEditDialog> {
  late TextEditingController _titleController;
  late TextEditingController _descriptonController;
  final _focusTextInput = FocusNode();
  final _descriptionFocusInput = FocusNode();

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController(text: widget.titleValue);
    _descriptonController =
        TextEditingController(text: widget.descriptionValue);
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = ref.read(todoProvider.notifier);

    return GestureDetector(
      onTap: () {
        if (_focusTextInput.hasFocus || _descriptionFocusInput.hasFocus) {
          _focusTextInput.unfocus();
          _descriptionFocusInput.unfocus();
        }
      },
      child: Container(
        padding: EdgeInsets.only(
            right: 26.w,
            left: 26.w,
            bottom: MediaQuery.of(context).viewInsets.bottom),
        color: AppStyle.backgroundColor,
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 25.h),
              child: Text(
                'Editar Tarea',
                textAlign: TextAlign.center,
                style: AppStyle.title1,
              ),
            ),
            TodoInputField(
              focusTextInput: _focusTextInput,
              controller: _titleController,
              message: "Agregar una tarea",
            ),
            SizedBox(height: 10.h),
            TodoInputField(
              focusTextInput: _descriptionFocusInput,
              controller: _descriptonController,
              message: "Agregar una descripcion a esta tarea",
            ),
            SizedBox(height: 30.h),
            CustomButton(
              onPress: () => Navigator.pop(context),
              text: 'Cancelar',
              borderColor: AppStyle.ligthColor,
            ),
            SizedBox(height: 20.h),
            CustomButton(
              text: "Confirmar",
              onPress: () {
                if (_titleController.text.isNotEmpty) {
                  viewModel.editTask(widget.index, _titleController.text,
                      _descriptonController.text);
                  Navigator.pop(context);
                }
              },
            ),
            SizedBox(height: 20.h),
          ]),
        ),
      ),
    );
  }
}
