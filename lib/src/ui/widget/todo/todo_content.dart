part of pruebar5.ui.widget;

class TodoContent extends ConsumerWidget {
  const TodoContent({required this.listOfTodos, super.key});
  final List<TodoModel>? listOfTodos;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final viewModel = ref.read(todoProvider.notifier);

    return Column(
      children: listOfTodos
              ?.map((todo) => TodoCard(
                    title: todo.title,
                    date: todo.date,
                    description: todo.description,
                    hasDone: todo.hasDone,
                    onDone: () =>
                        viewModel.doneTask(listOfTodos?.indexOf(todo) ?? 0),
                    onRemove: () =>
                        viewModel.removeTask(listOfTodos?.indexOf(todo) ?? 0),
                    onUpdate: () {
                      if (!todo.hasDone) {
                        showModalBottomSheet(
                          context: context,
                          backgroundColor: AppStyle.backgroundColor,
                          isScrollControlled: true,
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          )),
                          showDragHandle: true,
                          builder: (context) => TodoEditDialog(
                            titleValue: todo.title,
                            descriptionValue: todo.description,
                            index: listOfTodos?.indexOf(todo) ?? 0,
                          ),
                        );
                      }
                    },
                  ))
              .toList() ??
          [],
    );
  }
}
