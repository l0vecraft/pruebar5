part of pruebar5.ui.widget;

class TodoCard extends StatelessWidget {
  const TodoCard(
      {super.key,
      required this.title,
      required this.date,
      this.description,
      required this.onDone,
      required this.onRemove,
      required this.onUpdate,
      this.hasDone = false});

  final String title;
  final int date;
  final String? description;
  final VoidCallback onUpdate;
  final VoidCallback onRemove;
  final VoidCallback onDone;
  final bool hasDone;

  static final format = DateFormat("dd/MM/yyyy");

  @override
  Widget build(BuildContext context) {
    return FadeInLeft(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5.h),
        child: SizedBox(
          height: 120.h,
          child: Card(
            color: AppStyle.secondaryColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: const BorderSide(color: AppStyle.ligthColor)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 10.w),
                  child: Text(
                      format.format(DateTime.fromMillisecondsSinceEpoch(date)),
                      style: AppStyle.todoItemDescriptionStyle(hasDone)),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          CircularTodoStateWidget(
                              onDone: onDone, hasDone: hasDone),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  title,
                                  style: AppStyle.todoItemTitleStyle(hasDone),
                                ),
                                Text(
                                  description ?? 'Escribe una descripcion',
                                  style: AppStyle.todoItemDescriptionStyle(
                                      hasDone),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        if (!hasDone)
                          IconButton(
                              onPressed: onUpdate,
                              icon: Icon(
                                Icons.mode_edit_outline_outlined,
                                color: AppStyle.ligthColor,
                                size: 25.sp,
                              )),
                        IconButton(
                            onPressed: onRemove,
                            icon: Icon(
                              Icons.delete_outline_rounded,
                              color: AppStyle.ligthColor,
                              size: 25.sp,
                            ))
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
