part of pruebar5.ui.widget;

class TodoFormField extends ConsumerStatefulWidget {
  const TodoFormField({required this.focus, Key? key}) : super(key: key);
  final FocusNode focus;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TodoFormFieldState();
}

class _TodoFormFieldState extends ConsumerState<TodoFormField> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final viewModel = ref.read(todoProvider.notifier);
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 8.w),
            child: TodoInputField(
                message: "Agregar Tarea",
                focusTextInput: widget.focus,
                controller: _controller,
                onSubmit: (value) {
                  if (value.isNotEmpty) {
                    viewModel.addTodo(value);
                    _controller.clear();
                  }
                }),
          ),
        ),
        AddTodoButton(onPressed: () {
          if (_controller.text.isNotEmpty) {
            viewModel.addTodo(_controller.text);
            _controller.clear();
          }
          widget.focus.unfocus();
        }),
      ],
    );
  }
}
