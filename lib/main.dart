import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pruebar5/router/go_routes_config.dart';
import 'package:pruebar5/src/core/firebase_init.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FireBaseInit().initialize();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(390, 844),
        child: MaterialApp.router(routerConfig: GoRoutesConfig.routes));
  }
}
