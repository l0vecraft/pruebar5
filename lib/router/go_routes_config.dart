import 'package:go_router/go_router.dart';
import 'package:pruebar5/src/ui/screen/screen.dart';

class GoRoutesConfig {
  static var routes = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const HomeScreen(),
    )
  ]);
}
